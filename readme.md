
Time taken for total assignment is 2 hrs 45 mins.

Assignment :-

Using Laravel 5.8, code a media library application, both front end ui and backbend.
Through the application, the user should be able to upload or store content in the following formats:

- Jpeg, PNG
- Mp4
- PDF
- Mp3, 
- streamed content from YouTube or Vimeo

The user should be able to store, edit, delete and categorise the content into the following categories:

- Video
- Images
- Documents

Extra credit will be awarded for the following:

- store media meta data
- portability of the render code - this is so it could be used to render the media file easily in other parts of the application.
- front end using some Vue.js Elements
- good UI and UX 
- add a 'favourite' feature for users to store their favourite content
- Your ability will also be judged on the quality of your code, so please adhere to your best coding practices. Code quality will win over code quantity.

Laravel Skills to Demonstrate:

- Request Validation
- Collections
- Eloquent queries and relations
- Migrations and data seeding

Coding Skills to Demonstrate:
- OOP design - e.g: inheritance, patterns 
- SOLID principles
- MVC pattern adherence
- Testing (preferably Frontend and Backend)
