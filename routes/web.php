<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
	Route::get('content/create'					, 'ContentController@create');
	Route::get('content/manage/{option}/{page?}', 'ContentController@manage')->name('contents')->where('option', '(all|favourites)');
	Route::get('content/view/{id}'				, 'ContentController@view');
	Route::get('content/delete/{id}'			, 'ContentController@delete');
	Route::get('favourite/save/{id}'			, 'FavouriteController@save');
	Route::get('favourite/remove/{id}'			, 'FavouriteController@remove');

	Route::post('content/save'					, 'ContentController@save');

});

Route::get('/home', 'HomeController@index')->name('home');
