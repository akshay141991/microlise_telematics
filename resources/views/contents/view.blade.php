@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">View {{ $content->name }}</div>

                <div class="card-body">
                    @switch($content->category->category)
                        @case('Images')
                            <img src="{{ asset('storage/'.$content->path) }}" class="card-img-top" alt="{{ $content->name }}">
                            @break
                        @case('Documents')
                            <embed src="{{ asset('storage/'.$content->path) }}" type="application/pdf" width="100%" height="600px" />
                            @break
                        @case('Video')
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{ asset('storage/'.$content->path) }}" allowfullscreen></iframe>
                            </div>
                            @break
                    @endswitch
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
