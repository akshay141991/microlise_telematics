@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any() || session()->get('error'))
                        <div class="alert alert-danger">
                            Whoops! There was an error while submitting the form.
                            <ul>
                                @if ($errors->any())
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                @endif
                                @if (session()->get('error'))
                                    @foreach(session()->get('error') as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    @endif
                    <form action="{{ url('content/save') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Select file</label>
                            <input type="file" class="form-control-file" required name="content">
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            <select class="form-control" name="category" placeholder="Select Category">
                                @foreach ($categories as $category)
                                    <option value="{{ $category['id'] }}">{{ $category['category'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
