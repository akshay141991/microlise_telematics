@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">All Contents</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-hover table-bordered table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Category</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $content)
                                <tr>
                                    <th scope="row">{{ $content->id }}</th>
                                    <td>{{ $content->name }}</td>
                                    <td>{{ $content->category->category }}</td>
                                    <td>
                                        <ul class="nav justify-content-end">
                                            @if (Request::segment(3) == 'all')
                                                <li class="nav-item">
                                                    <a class="{{-- btn btn-primary --}}" href="{{ url('favourite/save', $content->id) }}">Favourite</a>
                                                </li> | 
                                            @else
                                                <li class="nav-item">
                                                    <a class="{{-- btn btn-primary --}}" href="{{ url('favourite/remove', $content->favourites->id) }}">Remove</a>
                                                </li> | 
                                            @endif
                                            <li class="nav-item">
                                                <a class="{{-- btn btn-primary --}}" href="{{ url('content/view', $content->id) }}">View</a>
                                            </li> | 
                                            <li class="nav-item">
                                                <a class="{{-- btn btn-danger --}}" href="{{ url('content/delete', $content->id) }}">Delete</a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $contents->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
