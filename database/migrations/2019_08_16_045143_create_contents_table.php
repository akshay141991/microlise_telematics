<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')
                    ->index();
            $table->string('path');
            $table->string('size')
                    ->index();
            $table->string('extension')
                    ->index();
            $table->bigInteger('category_id')
                    ->unsigned()
                    ->index();
            $table->timestamps();


            $table->index(['size', 'name']);
            $table->index(['extension', 'name']);
            $table->index(['category_id', 'name']);


            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
