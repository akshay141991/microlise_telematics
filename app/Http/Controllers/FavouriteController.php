<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Favourite;

use Auth;

class FavouriteController extends Controller
{
    public function save($id)
    {
    	$favourite['user_id'] 		= Auth::user()->id;
    	$favourite['content_id'] 	= $id;
    	$favourite 					= Favourite::firstOrCreate($favourite);

    	return redirect()->back()->with(['status' => 'File added to your favourite list.']);
    }

    public function remove($id)
    {
		Favourite::find($id)->delete();
    	return redirect()->back()->with(['status' => 'File removed from your favourite list.']);
    }
}
