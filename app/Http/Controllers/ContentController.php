<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContentRequest;

use App\Models\Content;

use Storage;

class ContentController extends Controller
{

	public function create()
	{
		$categories = (new CategoryController)->get();
		return view('contents.create', compact('categories'));
	}

	public function save(ContentRequest $request)
	{
		$path 					= $request->content->store('content');
		$content['name'] 		= $request->content->getClientOriginalName();
		$content['path'] 		= $path;
		$content['size'] 		= Storage::size($path);
		$content['extension'] 	= $request->content->getMimeType();
		$content['category_id'] = $request->category;
		$content 				= Content::create($content);

		return 	redirect()->back()->with(['status' => 'You have successfully uploaded the file.']);
	}

	public function manage($option)
	{
		switch ($option) {
			case 'all':
				$contents = Content::with('category')->paginate(5);
				break;
			case 'favourites':
				$contents = Content::with(['category', 'favourites'])->has('favourites')->paginate(5);
				break;
		}
		// dd($contents->toArray());
		return view('contents.manage', compact('contents'));
	}

	public function view($id)
	{
		$content = $this->find($id);
		return view('contents.view', compact('content'));

	}

	public function delete($id)
	{
		$content = $this->find($id);
		Storage::delete($content->path);
		$content->delete();
		return  redirect()->back()->with(['status' => 'File deleted successfully.']);
	}

	public function find($id)
	{
		return Content::find($id);
	}
}
