<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Content extends Model
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'path',
		'size',
		'extension',
		'category_id'
	];

	public function favourites()
	{
		return $this->hasOne('App\Models\Favourite')->where('user_id', '=', Auth::user()->id);
	}

	public function category()
	{
		return $this->belongsTo('App\Models\Category');
	}
}
